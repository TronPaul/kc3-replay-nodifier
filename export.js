const fs = require('fs');
const kceq = require('./kancolle-replay/js/kcEQDATA.js');
const kcship = require('./kancolle-replay/js/kcSHIPDATA.js');

if (!fs.existsSync('./out')) {
    fs.mkdirSync('./out');
}

const equipment_types = kceq.types.map(function(e, i) {
    return [e, kceq.types_values[i]];
}).reduce(function(map, arr) {
    map[arr[0]] = arr[1];
    return map;
}, {});

const equipment_secondary_types = kceq.secondary_types.map(function(e, i) {
    return [e, kceq.secondary_types_values[i]];
}).reduce(function(map, arr) {
    map[arr[0]] = arr[1];
    return map;
}, {});

const equipment_anti_air_types = kceq.anti_air_types.map(function(e, i) {
    return [e, kceq.anti_air_types_values[i]];
}).reduce(function(map, arr) {
    map[arr[0]] = arr[1];
    return map;
}, {});

fs.writeFileSync('out/eqiupment.json', JSON.stringify(kceq.EQDATA));
fs.writeFileSync('out/equipment-type.json', JSON.stringify(kceq.EQTDATA));
fs.writeFileSync('out/fit.json', JSON.stringify(kceq.FITDATA));
fs.writeFileSync('out/fit-night.json', JSON.stringify(kceq.FITDATAN));
fs.writeFileSync('out/special-improve.json', JSON.stringify(kceq.IMPROVESPECIAL));
fs.writeFileSync('out/equipment-types.json', JSON.stringify(equipment_types));
fs.writeFileSync('out/equipment-secondary-types.json', JSON.stringify(equipment_secondary_types));
fs.writeFileSync('out/equipment-anti-air-types.json', JSON.stringify(equipment_anti_air_types));
fs.writeFileSync('out/equipment-lbas.json', JSON.stringify(kceq.LBASDATA));

fs.writeFileSync('out/ships.json', JSON.stringify(kcship.SHIPDATA));
