#!/bin/bash
kc_sim="kancolle-replay/js/kcsim.js"

echo "" >> "$kc_sim"
grep -o '^var [^ ]\+' $kc_sim | sed 's/var //' | xargs -I{} -n 1 echo "exports.{} = {};" >> "$kc_sim"
grep -o '^function [^ (]\+' $kc_sim | sed 's/function //' | xargs -I{} -n 1 echo "exports.{} = {};" >> "$kc_sim"
grep -o '^const [^ ]\+' $kc_sim | sed 's/const //' | xargs -I{} -n 1 echo "exports.{} = {};" >> "$kc_sim"

