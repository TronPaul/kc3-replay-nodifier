#!/bin/bash
file=$1

sed -i '' 's/Math.random()/RAND_LOGGER()/' $file
echo >> $file
cat >>$file <<EOF
function RAND_LOGGER() {
    console.log(Error().stack);
    return Math.random();
}
EOF
