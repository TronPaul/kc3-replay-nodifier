#!/bin/bash
eq_data="kancolle-replay/js/kcEQDATA.js"
ship_data="kancolle-replay/js/kcSHIPDATA.js"

echo "" >> "$eq_data"
echo "exports.types = $(grep -o 'const [A-Z0-9_]\+' kancolle-replay/js/kcEQDATA.js | grep -v _ | grep -o '[A-Z0-9]\+' | jq -R . | jq --slurp -c .);" >> "$eq_data"
echo "exports.types_values = $(grep -o 'const [A-Z0-9_]\+' kancolle-replay/js/kcEQDATA.js | grep -v _ | grep -o '[A-Z0-9]\+' | jq -R . | jq --slurp -c . | sed 's/"//g');" >> "$eq_data"
echo "exports.secondary_types = $(grep -o 'const B_[A-Z0-9]\+' kancolle-replay/js/kcEQDATA.js | sed 's/const B_//' | jq -R . | jq --slurp -c .);" >> "$eq_data"
echo "exports.secondary_types_values = $(grep -o 'const B_[A-Z0-9]\+' kancolle-replay/js/kcEQDATA.js | sed 's/const //' | jq -R . | jq --slurp -c . | sed 's/"//g');" >> "$eq_data"
echo "exports.anti_air_types = $(grep -o 'const A_[A-Z0-9]\+' kancolle-replay/js/kcEQDATA.js | sed 's/const A_//' | jq -R . | jq --slurp -c .);" >> "$eq_data"
echo "exports.anti_air_types_values = $(grep -o 'const A_[A-Z0-9]\+' kancolle-replay/js/kcEQDATA.js | sed 's/const //' | jq -R . | jq --slurp -c . | sed 's/"//g');" >> "$eq_data"
grep -o 'var [A-Z0-9]\+' $eq_data | sed 's/var //' | xargs -I{} -n 1 echo "exports.{} = {};" >> "$eq_data"

echo "" >> "$ship_data"
grep -o 'var [A-Z0-9]\+' $ship_data | sed 's/var //' | xargs -I{} -n 1 echo "exports.{} = {};" >> "$ship_data"
